terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.0"
    }  
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "<= 2.0.0"
    }
    rancher2 = {
      source = "rancher/rancher2"
      version = ">= 1.10.0" 
      #access_key = "" //
      #secret_key = ""
    }
    local = {
      source = "hashicorp/local"
    }
  }
}
#
provider "gitlab" { 
  token = "PRIVATE_TOKEN"
  base_url = "https://gitlab.com/samueldeniz80/todo_app_gitlab/"
}

# Configure the AWS Provider.
provider "aws" {
  region = "us-east-1"
}

data "gitlab_project" "projectx" {
  id = "PROJECT_PATH"
}

#
resource gitlab_project_cluster "k3s" {
  project                       = data.gitlab_project.projectx.id
  name                          = "k3s_cluster"
  enabled                       = true
  kubernetes_api_url            = "https://PUB_IP:6443"
  kubernetes_token              = templatefile("./k3s_cluster/token.txt", { })
  kubernetes_ca_cert            = templatefile("./k3s_cluster/cert.txt", { })
  kubernetes_authorization_type = "rbac"
  environment_scope             = "*"

  #kubernetes_namespace          = "devops-19-dev"
  #project                       = "251"
  #domain                        = "publicip.nip.io"
  #management_project_id         = "19"
  #depends_on = [ time_sleep.wait ]
  
}
