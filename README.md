# Description
This project aims to create a GitlabCI pipeline to deploy the To-Do web application with Kubernetes (AWS Elastic Kubernetes Service - EKS) on AWS EC2 Instances by pulling the app images from the AWS Elastic Container Registry (ECR) repository.


# Gitlab authentication to push from local to remote repo
git remote add origin https://<access-token-name>:<access-token>@gitlab.com/myuser/myrepo.git

# push changes another branch
git push --set-upstream origin <branch-name>


