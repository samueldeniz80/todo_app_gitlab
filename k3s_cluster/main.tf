terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}


resource "aws_instance" "master" {
  ami                    = "ami-05ca09a9e46e360bd"  #Custom AMI from alpine-3.17.1-bios with cloudinit and cgroup initiated via userdata
  instance_type          = "t3a.medium"
  key_name               = module.ssh_key_pair.key_name       #the key name is personal
  vpc_security_group_ids = [aws_security_group.k3s_server.id] 
  user_data = base64encode(templatefile("${path.module}/server-userdata.tmpl", {
    token = random_password.k3s_cluster_secret.result
  }))

  tags = {
    Name = "todoApp-master-prjX" #choose whatever name
  }

  connection {
      type     = "ssh"
      user     = "alpine"
      private_key = module.ssh_key_pair.private_key
      host = self.public_ip
  }

  provisioner "file" {
    source      = "gitlab-admin-sa.yaml"
    destination = "/home/alpine/gitlab-admin-sa.yaml"
  } 
 
}

resource "aws_instance" "worker" {
  ami                    = "ami-05ca09a9e46e360bd"  #Custom AMI from alpine-3.17.1-bios with cloudinit and cgroup initiated via userdata
  count                  = 1
  instance_type          = "t3a.small"
  key_name               = module.ssh_key_pair.key_name #the key name is personal
  vpc_security_group_ids = [aws_security_group.k3s_agent.id] 
  user_data = base64encode(templatefile("${path.module}/agent-userdata.tmpl", {
    host  = aws_instance.master.private_ip,
    token = random_password.k3s_cluster_secret.result
  }))
  tags = {
    Name = "todoApp-worker-prjX" #choose whatever name
  }
}

module "ssh_key_pair" {
  source                = "cloudposse/key-pair/aws"
  namespace             = "todo"
  stage                 = "app"
  name                  = "key"
  ssh_public_key_path   = "./pem_key"
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
}

output master-pubip{
  value=aws_instance.master.public_ip
}
