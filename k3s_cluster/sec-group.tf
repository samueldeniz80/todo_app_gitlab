resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default-VPC"
  }
}


resource "aws_security_group" "k3s_server" {
  name = "k3s-server"
  description = "Allow 22 and 6443 inbound traffic"
  vpc_id = aws_default_vpc.default.id

  ingress {
      description      = "for ssh"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      #security_group_id = aws_security_group.k3s_server.id      
    }

    ingress {
      description      = "for k3s"
      from_port        = 6443
      to_port          = 6443
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      #security_group_id = aws_security_group.k3s_server.id      
    }


  egress {
      description      = "allow all"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
    }
  

  tags = {
    Name = "k3s-server-sg"
  }
}


resource "aws_security_group" "k3s_agent" {
  name = "k3s-agent"
  description = "Allow ONLY 22 inbound traffic"
  vpc_id = aws_default_vpc.default.id

  ingress {
      description      = "for ssh"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      #security_group_id = aws_security_group.k3s_server.id      
    }

  egress {
      description      = null
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
    }

  tags = {
    Name = "k3s-agent-sg"
  }
}
